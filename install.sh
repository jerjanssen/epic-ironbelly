#!/bin/sh
export TARGET_CC=$NDK_HOME/toolchains/llvm/prebuilt/linux-x86_64/bin/i686-linux-android29-clang
export TARGET_AR=$NDK_HOME/toolchains/llvm/prebuilt/linux-x86_64/bin/i686-linux-android-ar
JNI_LIBS=$(pwd)/android/app/src/main/jniLibs

cd rust
cargo build --target i686-linux-android --release

mkdir -p $JNI_LIBS/x86

ln -s $(pwd)/target/i686-linux-android/release/libwallet.a $JNI_LIBS/x86/libwallet.a
